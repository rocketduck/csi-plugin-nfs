import logging
import os
import subprocess
import tempfile
import time
import weakref
from pathlib import Path
from threading import Thread
from typing import Dict

from . import utils


class StorageBackend:
    def __init__(self, config):
        self.ready = False
        self.config = config
        self.storage_root = Path(tempfile.mkdtemp())
        self.logger = logging.getLogger(self.__class__.__name__)
        Thread(target=self._mount).start()
        weakref.finalize(self, self._cleanup)

    def _mount(self):
        while True:
            try:
                utils.mount(self.config, None, self.storage_root)
            except subprocess.CalledProcessError:
                self.logger.exception(
                    "Mounting target storage failed, retrying in 10 seconds"
                )
                time.sleep(10)
            else:
                self.ready = True
                break

    def _cleanup(self):
        try:
            if self.ready:
                utils.umount(self.storage_root)
            self.storage_root.rmdir()
        except OSError:
            pass

    def _safe_storage_path(self, path: str) -> Path:
        if ".." in path or path.startswith("/"):
            raise ValueError("Path injection tried?")
        if "/" in path and not self.config.allow_nested_volumes:
            raise ValueError("Path injection tried (see --allow-nested-volumes)?")
        return self.storage_root / path

    def volume_id_from_name(self, name: str) -> str:
        return name

    def create(self, name: str, parameters: Dict[str, str]) -> str:
        volume_id = self.volume_id_from_name(name)
        final_path = self._safe_storage_path(volume_id)
        final_path.mkdir(int(parameters.get("mode", "777"), 8), exist_ok=True)
        os.chown(
            final_path,
            int(parameters.get("uid", "-1")),
            int(parameters.get("gid", "-1")),
        )
        return volume_id

    def delete(self, volume_id: str) -> None:
        final_path = self._safe_storage_path(volume_id)
        if final_path.exists():
            subprocess.check_call(["rm", "-rf", final_path])

    def exists(self, volume_id: str) -> bool:
        final_path = self._safe_storage_path(volume_id)
        return final_path.exists() and final_path.is_dir()
