import argparse
import logging
import os
from concurrent import futures
from importlib.metadata import version

import grpc
from grpc_reflection.v1alpha import reflection

from csi_plugin_nfs import csi_pb2, csi_pb2_grpc

from .backend import StorageBackend
from .controller import ControllerServicer
from .identity import IdentityServicer
from .interceptor import LoggingInterceptor
from .node import NodeServicer

logger = logging.getLogger("csi")


VERSION = version("csi_plugin_nfs")


def serve():
    parser = argparse.ArgumentParser(prog="csi_plugin_nfs")
    parser.add_argument("--node-id", required=True)
    parser.add_argument("--endpoint", default="unix:///csi/csi.sock")
    parser.add_argument(
        "--type",
        default="node",
        choices=["node", "controller", "monolith"],
    )
    parser.add_argument(
        "--log-level",
        default="INFO",
        choices=["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG"],
    )
    parser.add_argument(
        "--nfs-server",
        help="Root directory to provision from, eg nfs_server:/root",
        required=True,
    )
    parser.add_argument("--mount-options", help="Default mount options")
    parser.add_argument(
        "--allow-nested-volumes",
        action=argparse.BooleanOptionalAction,
        help="Allow volumes to contain slashes in their name",
    )
    parser.add_argument("--version", action="version", version=f"%(prog)s {VERSION}")

    args = parser.parse_args()

    FORMAT = "%(asctime)-15s:%(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(level=args.log_level, format=FORMAT)

    interceptors = [LoggingInterceptor(logger)]
    server = grpc.server(
        futures.ThreadPoolExecutor(max_workers=10), interceptors=interceptors
    )
    service_names = [
        reflection.SERVICE_NAME,
        csi_pb2.DESCRIPTOR.services_by_name["Identity"].full_name,
    ]

    # Set a new umask so we can create directories as requested in the volume spec
    os.umask(0o000)
    backend = StorageBackend(args)
    csi_pb2_grpc.add_IdentityServicer_to_server(IdentityServicer(args, backend), server)
    if args.type in ["node", "monolith"]:
        csi_pb2_grpc.add_NodeServicer_to_server(NodeServicer(args, backend), server)
        service_names.append(csi_pb2.DESCRIPTOR.services_by_name["Node"].full_name)
    if args.type in ["controller", "monolith"]:
        csi_pb2_grpc.add_ControllerServicer_to_server(
            ControllerServicer(args, backend), server
        )
        service_names.append(
            csi_pb2.DESCRIPTOR.services_by_name["Controller"].full_name
        )
    reflection.enable_server_reflection(service_names, server)
    server.add_insecure_port(args.endpoint)

    logger.info(f"Starting server on {args.endpoint}")
    server.start()
    try:
        server.wait_for_termination()
    except KeyboardInterrupt:
        logger.info("Terminated server after KeyboardInterrupt")
