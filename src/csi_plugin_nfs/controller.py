import grpc

from csi_plugin_nfs import csi_pb2, csi_pb2_grpc

from . import utils
from .validators import RequiredField, VolumeNameValidator, validate


class ControllerServicer(utils.CSIServiceMixin, csi_pb2_grpc.ControllerServicer):
    def ControllerGetCapabilities(self, request, context):
        Cap = csi_pb2.ControllerServiceCapability
        return csi_pb2.ControllerGetCapabilitiesResponse(
            capabilities=[Cap(rpc=Cap.RPC(type=Cap.RPC.Type.CREATE_DELETE_VOLUME))]
        )

    @validate(
        RequiredField("volume_id", "volume_capabilities"),
        VolumeNameValidator("volume_id"),
    )
    def ValidateVolumeCapabilities(self, request, context):
        if not self.backend.exists(request.volume_id):
            context.abort(grpc.StatusCode.NOT_FOUND, "Volume does not exist")
        for cap in request.volume_capabilities:
            if not any([cap.HasField("block"), cap.HasField("mount")]):
                context.abort(
                    grpc.StatusCode.INVALID_ARGUMENT,
                    "Cannot have both mount and block access type be undefined",
                )
            if not cap.HasField("mount"):
                context.abort(
                    grpc.StatusCode.INVALID_ARGUMENT,
                    "The driver currently only supports volumes accessed via the filesystem API",
                )
        Confirmed = csi_pb2.ValidateVolumeCapabilitiesResponse.Confirmed
        return csi_pb2.ValidateVolumeCapabilitiesResponse(
            confirmed=Confirmed(
                volume_context=request.volume_context,
                volume_capabilities=request.volume_capabilities,
                parameters=request.parameters,
            )
        )

    @validate(RequiredField("name", "volume_capabilities"), VolumeNameValidator("name"))
    def CreateVolume(self, request, context):
        self.logger.info(f"Creating volume with name {request.name!r}")
        volume_id = self.backend.create(request.name, request.parameters)
        vol = csi_pb2.Volume(
            volume_id=volume_id, capacity_bytes=0, volume_context=request.parameters
        )
        return csi_pb2.CreateVolumeResponse(volume=vol)

    @validate(RequiredField("volume_id"), VolumeNameValidator("volume_id"))
    def DeleteVolume(self, request, context):
        self.logger.info(f"Deleting volume with id {request.volume_id!r}")
        self.backend.delete(request.volume_id)
        return csi_pb2.DeleteVolumeResponse()
