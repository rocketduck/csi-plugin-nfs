from textwrap import indent

from grpc_interceptor import ServerInterceptor


class LoggingInterceptor(ServerInterceptor):
    def __init__(self, logger):
        self.logger = logger
        super().__init__()

    def intercept(self, method, request, context, method_name):
        if not method_name.startswith("/csi."):
            return method(request, context)

        response = None
        try:
            args = indent(str(request), "  ")
            self.logger.debug(
                f"Executing method {method_name!r}, with request:\n{args}"
            )
            response = method(request, context)
        except Exception:
            self.logger.debug(f"Finished execution of method {method_name!r}")
            raise
        else:
            args = indent(str(response), "  ")
            self.logger.debug(
                f"Finished execution of method {method_name!r}, with response:\n{args}"
            )
        return response
