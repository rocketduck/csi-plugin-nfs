import logging
import os
import subprocess

import grpc

logger = logging.getLogger("csi.util")

TEST_MODE = "ROCKETDUCK_CSI_TEST" in os.environ


def mount(config, volid, target_path, read_only=False):
    target_path = str(target_path)

    src_path = config.nfs_server.rstrip("/")
    if TEST_MODE:
        cmd = ["bindfs", "--no-allow-other"]
    else:
        cmd = ["mount", "-t", "nfs"]
        if ":" not in src_path:  # bare server name
            src_path += ":"
    src_path += f"/{volid or ''}"

    options = [config.mount_options] if config.mount_options else []
    if read_only:
        options.append("ro")
    if options:
        cmd.extend(["-o", ",".join(options)])
    cmd.extend([src_path, target_path])
    try:
        logger.debug(f"Executing mount command: {cmd!r}")
        subprocess.check_output(cmd, stderr=subprocess.STDOUT)
    except subprocess.SubprocessError as e:
        logger.error(f"Mounting failed with stdout/stderr: {e.stdout.decode().strip()}")
        raise


def umount(target_path):
    umount_cmd = ["fusermount", "-u"] if TEST_MODE else ["umount"]
    umount_cmd.append(str(target_path))
    try:
        logger.debug(f"Executing umount command: {umount_cmd!r}")
        subprocess.check_output(umount_cmd, stderr=subprocess.STDOUT)
    except subprocess.SubprocessError as e:
        logger.error(
            f"Unmounting failed with stdout/stderr: {e.stdout.decode().strip()}"
        )
        raise


def check_mount(target_path):
    cmd = [
        "findmnt",
        "--output",
        "TARGET,OPTIONS",
        "--noheadings",
        "--mountpoint",
        target_path,
    ]
    try:
        result = subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        return True, result.split(None, 1)
    except subprocess.SubprocessError:
        return False, []


class CSIServiceMixin:
    def __init__(self, config, backend):
        self.config = config
        lname = self.__class__.__name__.lower().replace("servicer", "")
        self.logger = logging.getLogger(lname)
        self.backend = backend
        super().__init__()

    def set_grpc_error(self, location, context):
        context.set_code(grpc.StatusCode.INTERNAL)
        context.set_details(
            f"{location} failed, check plugin logs on {self.config.node_id}"
        )
