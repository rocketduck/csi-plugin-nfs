Example JOB files for Nomad
===========================

This directory provides example job files when using this CSI driver inside Nomad.

Usage
-----

As with any other CSI driver, nomad needs to be configured to allow priviliged execution of containers. This is required so the CSI driver can actually mount things. Add the following configuration to your client configs:

```
plugin "docker" {
    config {
        allow_privileged = true
    }
}
```

Before the plugin can be deployed, the example files need to be adjusted to your needs. There are basically two things that need changing:

 * `--nfs-server=nfs_server:/share` Needs to be adjusted to match your server.
 * The id in your `csi_plugin` stanza should be set to something unique to your system.

Now you should be able to deploy the plugin:

```
nomad job run controller.nomad
nomad job run node.nomad
```

Next we create an example volume and job (If you adjusted the CSI plugin ID, make sure to also update `example.nomad`):
```
nomad volume create example.volume
nomad job run example.nomad
```

For more details on how to configure the CSI driver itself see the top-level README, this document solely focuses on the integration into Nomad.
