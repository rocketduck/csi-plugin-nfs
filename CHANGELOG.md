v1.1.0 (05.03.2025)
-------------------

 * Keep trying to mount the storage so the plugin doesn't stay unhealthy indefinetely after the first failure (#18).

v1.0.0 (28.02.2025)
-------------------

 * Set umask to 0o000 to ensure the requested mode is set (#17).

v0.7.0 (26.07.2023)
-------------------

 * Add support for nested volumes (containing slahes) via `--allow-nested-volumes`.

v0.6.1 (10.01.2023)
-------------------

 * Dependency updates.
 * Handle management mount in the background and only return ready once the backend mounted.

v0.5.0 (30.09.2022)
-------------------

 * Support '[]' in volume names for per\_alloc mounts.
 * Dependency updates.

v0.4.0 (23.05.2022)
-------------------

 * Dependency updates and internal restructuring.

v0.3.0 (22.05.2021)
-------------------

 * Properly implemented `ValidateVolumeCapabilities`.
 * Added request/response logging when the log level is DEBUG.

v0.2.0 (02.01.2021)
-------------------

 * Multi-Arch docker images, linux/amd64 and linux/arm64 for now (#2).
 * Support uid/gid/mode during volume creation (#1).

v0.1.0 (01.01.2021)
-------------------

 * Initial release, not much to say :)
